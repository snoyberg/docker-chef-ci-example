FROM fpco/pid1:20.04 as tooling
ARG TARGET=x86_64-unknown-linux-musl
ARG USERID
ARG GROUPID

RUN DEBIAN_FRONTEND=noninteractive apt-get update && apt-get install -yq \
    build-essential \
    musl-dev \
    musl-tools \
    curl \
    && \
    apt-get clean && rm -rf /var/lib/apt/lists/*

RUN curl https://s3.amazonaws.com/download.fpcomplete.com/cargo-chef/cargo-chef-741f9a2a4d4749d4371a67dd132de9d8d3211ed6-x86_64-unknown-linux-musl > /usr/bin/cargo-chef && chmod +x /usr/bin/cargo-chef

RUN groupadd --gid $GROUPID builder && \
    useradd --uid $USERID --gid $GROUPID builder && \
    mkdir -p /home/builder/src && \
    chown -R builder /home/builder
USER builder

COPY rust-toolchain /etc/rust-toolchain
RUN curl https://sh.rustup.rs -sSf | \
    sh -s -- -y \
        --default-toolchain $(cat /etc/rust-toolchain | tr -d '\r\n') \
        --target $TARGET \
        --no-modify-path
ENV PATH=/home/builder/.cargo/bin:$PATH
ENV CARGO_TARGET_DIR=/home/builder/target
WORKDIR /home/builder/src

FROM tooling as planner

COPY --chown=builder:builder . /home/builder/src
RUN cargo chef prepare --recipe-path ../recipe.json

FROM tooling
ARG TARGET=x86_64-unknown-linux-musl

COPY --from=planner /home/builder/recipe.json /home/builder/recipe.json
RUN cargo chef cook --all-targets --target $TARGET --release         --recipe-path ../recipe.json
RUN cargo chef cook --all-targets --target $TARGET --release --check --recipe-path ../recipe.json
