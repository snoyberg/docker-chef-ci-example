FROM registry.gitlab.com/snoyberg/docker-chef-ci-example/base-build:sha-8dc3b45f45149c5fd0b36261017d947b6b6cd12b as build

ARG TARGET=x86_64-unknown-linux-musl

COPY --chown=builder:builder . /home/builder/src

# Make sure build.rs is run to generate files
RUN find . -name build.rs -exec touch {} \;

RUN cargo fmt --all -- --check

RUN cargo test   --release --target $TARGET --target-dir $CARGO_TARGET_DIR
RUN cargo clippy --release --target $TARGET --target-dir $CARGO_TARGET_DIR -- --deny "warnings"
RUN cargo build  --release --target $TARGET --target-dir $CARGO_TARGET_DIR

RUN cp /home/builder/target/$TARGET/release/docker-chef-ci-example /home/builder

FROM registry.gitlab.com/snoyberg/docker-chef-ci-example/base-runtime:sha-8dc3b45f45149c5fd0b36261017d947b6b6cd12b
COPY --from=build /home/builder/docker-chef-ci-example /usr/bin/
CMD ["/usr/bin/docker-chef-ci-example"]
