#!/bin/sh

set -eux

rm -rf _artifacts
mkdir -p _artifacts

if [ "$(id -u)" = "0" ]
then
    USERID=1000
    GROUPID=1000
    chown $USERID:$GROUPID _artifacts
fi

cd $(dirname "$0")/..

docker build . -f etc/runtime.Dockerfile --tag $IMAGE
docker push $IMAGE

docker run --rm -v $(pwd)/_artifacts:/artifacts $IMAGE cp /usr/bin/docker-chef-ci-example /artifacts
