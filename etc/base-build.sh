#!/bin/sh

set -eux

USERID=$(id -u)
GROUPID=$(id -g)
if [ "$USERID" = "0" ]
then
    USERID=1000
    GROUPID=1000
fi

cd $(dirname "$0")/..

docker build . -f etc/base-build.Dockerfile \
    --tag $IMAGE \
    --build-arg USERID=$USERID \
    --build-arg GROUPID=$GROUPID
docker push $IMAGE
