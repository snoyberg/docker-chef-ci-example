FROM fpco/pid1:20.04

RUN DEBIAN_FRONTEND=noninteractive apt-get update && apt-get install -yq ca-certificates && \
    apt-get clean && rm -rf /var/lib/apt/lists/*
