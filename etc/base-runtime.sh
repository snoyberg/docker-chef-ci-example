#!/bin/sh

set -eux

cd $(dirname "$0")

docker build . -f base-runtime.Dockerfile --tag $IMAGE
docker push $IMAGE
