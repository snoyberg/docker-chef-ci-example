# Docker/cargo-chef/GitLab CI example

[![pipeline status](https://gitlab.com/snoyberg/docker-chef-ci-example/badges/master/pipeline.svg)](https://gitlab.com/snoyberg/docker-chef-ci-example/-/commits/master)

This repo demonstrates a CI technique I mentioned in a [cargo-chef pull request](https://github.com/LukeMathWalker/cargo-chef/pull/72#event-4974889505). The idea is to leverage `cargo-chef` to build a base Docker image that contains all of the build dependencies of a project. Then we use that image for doing all of the builds of the repo itself. This bypasses any need to build dependencies in the main build, letting it run in a much shorter period of time.

Why not simply use CI caching? That's certainly an option, and a valid one in many cases. But as we all know, there are two hard problems in computer science: naming things, cache invalidation, and off by one errors. Personally, I prefer the idea of having a well known, well named base image that contains all dependencies, instead of relying on caches and constructing the correct cache key. But YMMV, this is definitely an opinionated approach.

Breakdown of how this works:

* We generate two different base images: `base-build` contains all of the build tools an dependencies, and `base-runtime` installs necessary system libraries to run the application.
* Each of these images is built using its own `Dockerfile`, and initiated via a shell script, both of which are located in the `etc` directory
* The `base-runtime` setup is fairly simple, and not worth reviewing
* `base-build` is the interesting part. We have the following stages:
    * `tooling` takes a plain Ubuntu image and installs system libraries, copies the `cargo-chef` executable, adds a `builder` user account, installs the Rust toolchain, and sets up some environment variables
    * `planner` builds on `tooling`, and uses `cargo-chef` to produce a `recipe.json` file
    * Finally, `base-build` builds on `tooling`, and pulls in the `recipe.json` file to build `--check` (for clippy) and `--release` versions of the dependencies
* `.gitlab-ci.yml` is set up to run both `base-build` and `base-runtime` any time someone pushes to the `base` branch. This could alternatively be set up with manual builds or other triggers. Personally, I like the explicitness of pushing to a separate branch.
* The `runtime` Dockerfile pins exact tags of both `base-build` and `base-runtime`, and uses a multistage build process
    * First stage is built on `base-build`, and adds in the source files, runs clippy/fmt/test/build, and copies the resulting executable to a well known location
    * The second stage copies the executable from the previous step into the `base-runtime` image
* And finally, `runtime.sh` will copy out the resulting executable from the image so it's available for GitLab artifacts

Interesting things along the way:

* I'm using a musl-based build to get static executables
* I used the `--target-dir` parameter to ensure that the `target` directory was not a subdirectory of the source directory. This may not be strictly necessary, but it simplified any needs around copying that target directory between images. I found that to be a pretty slow process before.
* For some reason, it seems like `build.rs` doesn't get recompiled unless I explicitly update the timestamp, thus a `touch`. This repo doesn't actually include a `build.rs`, but the project I originally wrote this for does.
* In theory, we could totally bypass the need for separate base build steps if we could leverage `--cache-from` properly. However, I couldn't get that to work well with GitLab's registry.
